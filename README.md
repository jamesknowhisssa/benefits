Social Security Office in Washington (95 W Beau St 15301)
Driving Directions
Located On Ground Floor Of Bldg. Enter Bldg Through Revolving Doors. Take Elevators In Lobby To Ground Floor. Exit Left Off Elevators. Located Near Washington County Courthouse.
Social Security Office Location:
95 W Beau St
Washington
Pennsylvania
15301
United States
Social Security Office Hours
Mondays: 9:00am - 4:00pm
Tuesdays: 9:00am - 4:00pm
Wednesdays: 9:00am - 12:00pm
Thursdays: 9:00am - 4:00pm
Fridays: 9:00am - 4:00pm
Saturdays: Closed
Sundays: Closed
You can take care of these things without making an appointment at your local office.

Apply for Benefits
- Apply for Retirement Benefits
- Apply for Disability Benefits
- Apply for Medicare Benefits
- Appeal a Decision

Check Your Account Information
- View Your Latest SS Statement
- Review Your Earnings History
- Estimate Your Retirement Benefits
- Check Your Application Status

Updates To Your Account
- Change Your Address
- Direct Deposit Setup and Changes
- Print Proof of Benefits
- Print Out Your 1099 Form
<a href="https://ssa-office.com/org/social-security-office-in-washington-95-w-beau-st-15301/">https://ssa-office.com/org/social-security-office-in-washington-95-w-beau-st-15301/</a>